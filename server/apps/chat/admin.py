from django.contrib import admin
from apps.chat.models import ChatRoom, ChatMessage

# admin.site.register(ChatRoom)
# admin.site.register(ChatMessage)


@admin.register(ChatRoom)
class ChatRoom(admin.ModelAdmin):
    list_display = ['name', 'roomId', 'type']


@admin.register(ChatMessage)
class ChatMessage(admin.ModelAdmin):
    list_display = ['chat', 'user', 'message', 'timestamp']
